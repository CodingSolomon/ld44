﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Program: MonoBehaviour {

	public GameObject[] Pipes = new GameObject[16];
	public Sprite[] inactiveSprites;
	public Sprite[] activeSprites;

	float arrayPowerNum;
	float pipeWidth;
	float pipeHeight;

	private void Start() {
		arrayPowerNum = (float) Math.Pow(Pipes.Length, 0.5f);
		pipeWidth = 875 / arrayPowerNum;
		pipeHeight = 800 / arrayPowerNum;

		for (int i = 0; i < Pipes.Length; i++) {
			Transform hi = this.transform;
			Pipes[i] = Instantiate(Resources.Load("Pipe"), PosCalc(i) + this.transform.position, Quaternion.Euler(0,0,0)) as GameObject;
			Pipes[i].transform.SetParent(transform);
			Pipes[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, pipeWidth);
			Pipes[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, pipeHeight);
			Pipes[i].GetComponent<Image>().sprite = inactiveSprites[UnityEngine.Random.Range(0, 5)];
		}
	}

	Vector3 PosCalc(int CurrentTile) {

		float X = 0;
		float Y = 0;
		float Z = 0;

		X = CurrentTile * pipeWidth;

		while (X >= arrayPowerNum * pipeWidth) {
			X -= arrayPowerNum * pipeWidth;
			Y -= pipeHeight;
		}

		return new Vector3(X - (437.5f - pipeWidth / 2), Y + (387.5f - pipeHeight / 2), Z);
	}



	static void Main(string[] args) {
		Pipe testPipe = new Pipe(Pipe.Shape.L, new int[2] { 2, 3 }, 80);
		testPipe.getAngle();
		testPipe.getPosition();
		testPipe.setAngle(1000);
	}
}