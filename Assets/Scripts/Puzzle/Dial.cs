﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dial : MonoBehaviour{

	public Quaternion originalRotation;
	public float rotation = 0;
	public bool IsOver;

	public void Start() {
		originalRotation = this.transform.rotation;
	}

	private void Update() {
		if (IsOver) {
			if (Input.GetMouseButton(0)) {
				originalRotation = this.transform.rotation;
				Vector3 vector = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position); ;
				rotation = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
			}
		}

		this.transform.rotation = Quaternion.Euler(0,0, rotation);
	}

	void OnMouseEnter() {
		IsOver = true;
	}

	void OnMouseExit() {
		IsOver = false;
	}
}
