using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace LD45 {
	public class SignalPuzzle : MonoBehaviour {
		// inspector stuff
		public Material SignalMaterial;
		public MeshRenderer Renderer;

		public GameObject DialA;
		public GameObject DialB;
		public GameObject DialC;

		public float[] CorrectValues = new float[3];

		public Vector3 Wave;

		public float Y;
		bool Entered;

		KeyCode[] NumPad = { KeyCode.Keypad0, KeyCode.Keypad1, KeyCode.Keypad2, KeyCode.Keypad3, KeyCode.Keypad4, KeyCode.Keypad5, KeyCode.Keypad6, KeyCode.Keypad7, KeyCode.Keypad8, KeyCode.Keypad9 };
		KeyCode[] Alpha = { KeyCode.Alpha0, KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9 };

		public AudioSource EffectsAudioSource;
		public AudioSource NumberAudioSource;
		public AudioSource StaticAudioSource;

		public AudioClip[] Numbers;

		public AudioClip Success;
		public AudioClip Complete;
		public AudioClip Fail;
		public AudioClip Boom;

		public GameObject BoomImage;
		public GameObject BoomText;

		// state

		public float A {
			set {
				SetValue("_A", Mathf.Clamp01(value));
			}
			get { return GetValue("_A"); }
		}
		
		public float B {
			set {
				SetValue("_B", Mathf.Clamp01(value));
			}
			get { return GetValue("_B"); }
		}
		
		public float C {
			set {
				SetValue("_C", Mathf.Clamp01(value));
			}
			get { return GetValue("_C"); }
		}

		public int CodeIndex { get; private set; } = 0;

		public int[] Code { get; private set;  }

		void Awake() {
			Renderer.material = new Material(SignalMaterial);

			Code = GetRandomCode();

			SetNewRandomWave();
		}

		private void Update() {
			Wave = WaveForm(DialA.transform.rotation.eulerAngles.z, DialB.transform.rotation.eulerAngles.z, DialC.transform.rotation.eulerAngles.z);
			A = Wave.x;
			B = Wave.y;
			C = Wave.z;
			Y = (float)Math.Pow(Math.Pow(Wave.x - CorrectValues[0], 2) + Math.Pow(Wave.y - CorrectValues[1], 2) + Math.Pow(Wave.z - CorrectValues[2], 2), 0.5f) / (float)Math.Pow(3, 0.5f);
			StaticAudioSource.volume = Y;

			if (Y < 0.03f) {
				if (!Entered) {
					if (!NumberAudioSource.isPlaying) {
						NumberAudioSource.loop = true;
						NumberAudioSource.clip = Numbers[Code[CodeIndex]];
						NumberAudioSource.Play();
					}
				}
			}
			else {
				NumberAudioSource.Stop();
			}

			for (int i = 0; i < 10; i++) {
				if (Input.GetKeyDown(NumPad[i]) || Input.GetKeyDown(Alpha[i])) {
					StartCoroutine(EnterDigit(i));
				}
			}
		}

		// ui hook stuff

		IEnumerator EnterDigit(int digit) {
			if (Code[CodeIndex] == digit) {
				NumberAudioSource.loop = false;
				NumberAudioSource.clip = Success;
				NumberAudioSource.Play();
				Entered = true;
				yield return new WaitForSeconds(3);
				if (CodeIndex == 3) {
					NumberAudioSource.clip = Complete;
					NumberAudioSource.Play();
					StaticAudioSource.Stop();
					yield return new WaitForSeconds(7);
					SceneManager.LoadScene(0);
				}
				else {
					Entered = false;
					CodeIndex++;
					SetNewRandomWave();
				}
			} else {
				StaticAudioSource.Stop();
				StaticAudioSource.loop = false;
				StaticAudioSource.clip = Fail;
				StaticAudioSource.Play();
				yield return new WaitForSeconds(2);
				BoomImage.SetActive(true);
				StaticAudioSource.loop = false;
				StaticAudioSource.clip = Boom;
				StaticAudioSource.Play();
				yield return new WaitForSeconds(3);
				BoomText.SetActive(true);
				yield return new WaitForSeconds(3);
				SceneManager.LoadScene(0);
			}
		}

		//ui dial determination
		public Vector3 WaveForm(float drA, float drB, float drC) {
			return new Vector3(drA / 360, drB / 360, drC / 360);
		}
		
		// utils
		
		static int[] GetRandomCode() {
			return Enumerable.Range(0, 4).ToList().ConvertAll(a => Random.Range(0, 10)).ToArray();
		}
		
		private void SetValue(string property, float value) {
			Renderer.material.SetFloat(Shader.PropertyToID(property), value);
		}

		private float GetValue(string property) {
			return Renderer.material.GetFloat(Shader.PropertyToID(property));
		}

		private void SetNewRandomWave() {
			A = Random.Range(0, 11) * 0.1f;
			B = Random.Range(0, 11) * 0.1f;
			C = Random.Range(0, 11) * 0.1f;
			CorrectValues[0] = Random.Range(0, 11) * 0.1f;
			CorrectValues[1] = Random.Range(0, 11) * 0.1f;
			CorrectValues[2] = Random.Range(0, 11) * 0.1f;
		}
	}
}