﻿using System;
using UnityEngine;

public class Pipe : MonoBehaviour {
		public Pipe(Shape shape, int[] position, int angle) {
			this.shape = shape;
			setPosition(position);
			setAngle(angle);
		}

		public bool setAngle(int angleDeg) {
			//keep between 0 and 360
			while (angleDeg < 0)
				angleDeg += 360;
			while (angleDeg >= 360)
				angleDeg -= 360;

			//lock into 90-degree increments
			if (angleDeg >= 270)
				this.angleDeg = 270;
			else if (angleDeg >= 180)
				this.angleDeg = 180;
			else if (angleDeg >= 90)
				this.angleDeg = 90;
			else
				this.angleDeg = 0;

			//no way to return false currently
			return true;
		}

		public bool setPosition(int[] position) {
			if (position.Length == 2) {
				this.position = position;
				return true;
			}
			else {
				Console.WriteLine("Error: position must be two numbers in the format, (row,column)\n");
				return false;
			}
		}

		public bool rotate() {
			setAngle(angleDeg + 90);
			return true;
		}

		//@return angleDeg
		public int getAngle() {
			return angleDeg;
		}

		//@return position
		public int[] getPosition() {
			return position;
		}

		public bool isSolved() {
			if (/*TODO: path is complete*/true) {
				return true;
			}
			return false;
		}

		public void draw() {
			/*TODO: add basic shape-drawing code*/
			/*TODO: add images based on shape of pipe*/
			/*TODO: write code to rotate drawn image over time based on lastDrawnAngle and current angleDeg*/
		}


		public enum Shape { T, L, DOUBLE_L, STRAIGHT, CROSS };
		private int angleDeg;
		private int[] position = new int[2];
		private Shape shape;
		private int lastDrawnAngle;
	}