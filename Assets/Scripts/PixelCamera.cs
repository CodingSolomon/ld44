﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelCamera : MonoBehaviour
{
    [SerializeField]
    private float pixelSize;
    private Material pixelateMaterial;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if(pixelateMaterial == null)
        {
            pixelateMaterial = new Material(Shader.Find("Hidden/PixelateShader"));
        }

        pixelateMaterial.SetFloat("_PixWidth", 1.0f/ (Screen.width/ pixelSize));
        pixelateMaterial.SetFloat("_PixHeight", 1.0f/ (Screen.height / pixelSize));

        Graphics.Blit(source, destination, pixelateMaterial);
    }
}
