﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAudio : MonoBehaviour {

	public Animator BossAnimator;

	public AudioSource BossAS;

	public AudioClip[] idleClips;

	public AudioClip[] successClips;

	public AudioClip[] failClips;

	int failedNum;
	int time;
	float timer;

	private void Start() {
		BossAnimator = this.GetComponent<Animator>();
	}

	void Update(){
		if (!BossAS.isPlaying) {
			if (timer >= time) {
				BossAS.clip = idleClips[Random.Range(0, idleClips.Length)];
				BossAS.Play();
				time = Random.Range(5,15);
				timer = 0;
			}
			else {
				timer += Time.deltaTime;
			}
			BossAnimator.SetBool("Talking", false);
		}
		else {
			BossAnimator.SetBool("Talking", true);
		}
    }
}