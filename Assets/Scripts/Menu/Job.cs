﻿using UnityEngine;

[System.Serializable]
public class Job{
	public string name;
	public string time;
	public int pay;
	public string description;

	public static Job CreateFromJSON(string jsonString) {
		return JsonUtility.FromJson<Job>(jsonString);
	}
}