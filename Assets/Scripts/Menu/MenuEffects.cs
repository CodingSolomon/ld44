﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuEffects : MonoBehaviour{

	[Header("Paralax")]
	public Transform Background;
	Vector3 BackgroundOriginPosition;
	public Transform Boss;
	Vector3 BossOriginPosition;

	[Header("Sounds")]
	AudioSource CanvasAudio;
	public AudioClip Click1;

	void Start(){
		CanvasAudio = this.GetComponent<AudioSource>();
		BossOriginPosition = Boss.transform.position;
		BackgroundOriginPosition = Background.transform.position;
	}

    void Update(){
        Vector2 mousePos = new Vector2((Input.mousePosition.x/Screen.width*2)-1, (Input.mousePosition.y / Screen.height * 2) - 1);
		Background.transform.position = new Vector3(BackgroundOriginPosition.x + mousePos.x/2, BackgroundOriginPosition.y + mousePos.y / 2, 0);
		Boss.transform.position = new Vector3(BossOriginPosition.x + mousePos.x / 4, BossOriginPosition.y + mousePos.y /4, 0);
	}

	public void PlayClickSound() {
		CanvasAudio.clip = Click1;
		CanvasAudio.Play();
	}

	public void LoadLevel() {
		SceneManager.LoadScene(1);
	}

	public void ExitApp() {
		Application.Quit();
	}
}