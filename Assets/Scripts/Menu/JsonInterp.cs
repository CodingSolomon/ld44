﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class JsonInterp : MonoBehaviour {
	string path;
	string jsonString;

	public Jobs JsonJobs;

	public GameObject JobContainer;
	public GameObject JobDes;

	[Header("Saved Data")]
	int[] jobNums;

	void Start() {
		//Load Jobs JSON
		path = Application.dataPath + "/Resources/jobs.json";
		jsonString = File.ReadAllText(path);
		JsonJobs = JsonUtility.FromJson<Jobs>(jsonString);

		//Load Import JSON data to text fields
		//for (int i = 0; i < JsonJobs.jobs.Length; i++) {
			JobContainer.transform.Find("Text").GetComponent<Text>().text = " " + JsonJobs.jobs[0].name;
			JobContainer.transform.Find("Time").GetComponent<Text>().text = JsonJobs.jobs[0].time + " ";
			JobContainer.transform.Find("Price").GetComponent<Text>().text = "$" + JsonJobs.jobs[0].pay + " ";
		//}
	}

	public void LoadPage(int jobNum) {
		JobDes.SetActive(true);
		JobDes.transform.Find("Text").GetComponent<Text>().text = " " + JsonJobs.jobs[jobNum].name;
		JobDes.transform.Find("Time").GetComponent<Text>().text = JsonJobs.jobs[jobNum].time + " ";
		JobDes.transform.Find("Price").GetComponent<Text>().text = "$" + JsonJobs.jobs[jobNum].pay + " ";
		JobDes.transform.Find("Dis").GetComponent<Text>().text = JsonJobs.jobs[jobNum].description;
	}

	public void CancelMissionSelect() {
		JobDes.SetActive(false);
	}

	[System.Serializable]
	public class Jobs {
		public Job[] jobs;
	}
}