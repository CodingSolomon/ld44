﻿Shader "Unlit/SignalShader"
{
    Properties {
        _MainTex ("Texture", 2D) = "white" {}
        _Thickness ("Thickness", Float) = 1
        _BackgroundColor ("Background Color", Color) = (0, 0, 0, 1) 
        _ForegroundColor ("Foreground Color", Color) = (1, 1, 1, 1)
        _PixelSize ("Pixel Size", Float) = 30
        _A ("A", Range (0, 1)) = 0
        _B ("B", Range (0, 1)) = 0
        _C ("C", Range (0, 1)) = 0
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work

            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            
            float _Thickness;
            
            float4 _BackgroundColor;
            float4 _ForegroundColor;
            
            float _PixelSize;
            
            float _A;
            float _B;
            float _C;

            v2f vert(appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target {
                float x = floor(i.uv.x*_PixelSize)/_PixelSize;
                float y = floor(i.uv.y*_PixelSize)/_PixelSize;
                
                float t = x*6;
                
                // the epitome of readability
                float value = (_C*25+2)/(10/3)*sin(.5/(_B*.5+.6)*t - _Time.w*1.25) + (_A+.5)*1.2*sin((_A + 1.5   )*t + _Time.w*1.25);
                value /= 5;
                
                value = smoothstep(-1, 1, value*.5);
                
                float distance = abs(y - value);
                
                distance = clamp(distance, 0, .025 * _Thickness);
                distance = pow((.025 * _Thickness - distance)*40, 10);
                                                
                return lerp(_BackgroundColor, _ForegroundColor, distance);
            }
            ENDCG
        }
    }
}
